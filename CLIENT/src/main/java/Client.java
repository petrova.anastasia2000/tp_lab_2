import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args){
        try(
            Socket socket = new Socket("localhost", 12345);
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream())))
        {
            System.out.println("Waiting...");
            String roleJson = reader.readLine();
            System.out.println("JSON role - " + roleJson);

            Gson gson = new Gson();
            Role role = gson.fromJson(roleJson, Role.class);
            System.out.printf("You are %s!\n", role.name());

            Scanner scanner = new Scanner(System.in);
            if (role.equals(Role.WISHER)) {
                System.out.print("Wish the word> ");
                String word = scanner.nextLine();
                writer.println(word);
            }else{
                System.out.println("Wait...");
            }

            while(true) {
                String anotherMoveJson = reader.readLine();
                System.out.println("JSON move - " + anotherMoveJson);
                Move anotherMove = gson.fromJson(anotherMoveJson, Move.class);
                System.out.printf("%s: %s\n", anotherMove.getType().name(), anotherMove.getMessage());

                if(anotherMove.getType().equals(Move.Type.SERVER_END)){
                    break;
                }

                System.out.print("YOU: ");
                String msg = scanner.nextLine();
                Move myMove = new Move(msg, Move.Type.PLAYER);
                String myMoveJson = gson.toJson(myMove);
                writer.println(myMoveJson);
            }

        }catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
}
