import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(12345);
             Socket wisherSocket = serverSocket.accept();
             Socket guesserSocket = serverSocket.accept();
             PrintWriter wisherWriter =
                     new PrintWriter(wisherSocket.getOutputStream(), true);
             BufferedReader wisherReader =
                     new BufferedReader(new InputStreamReader(wisherSocket.getInputStream()));
             PrintWriter guesserWriter =
                     new PrintWriter(guesserSocket.getOutputStream(), true);
             BufferedReader guesserReader =
                     new BufferedReader(new InputStreamReader(guesserSocket.getInputStream()))
        ) {
            Gson gson = new Gson();

            wisherWriter.println(gson.toJson(Role.WISHER));
            guesserWriter.println(gson.toJson(Role.GUESSER));

            final String word = wisherReader.readLine();
            System.out.println(word);

            Move move = new Move("word wished", Move.Type.SERVER_START);
            String wishedWordJson = gson.toJson(move);
            guesserWriter.println(wishedWordJson);

            while (true) {
                String guesserMoveJson = guesserReader.readLine();
                Move guesserMove = gson.fromJson(guesserMoveJson, Move.class);

                if (guesserMove.getMessage().equalsIgnoreCase(word)) {
                    Move endGameMove = new Move("Game over!", Move.Type.SERVER_END);
                    String endGameJson = gson.toJson(endGameMove);
                    guesserWriter.println(endGameJson);
                    wisherWriter.println(endGameJson);
                    break;
                }
                wisherWriter.println(guesserMoveJson);

                String wisherMoveJson = wisherReader.readLine();
                guesserWriter.println(wisherMoveJson);
            }
        } catch (IOException exception) {
            System.out.println(exception.getMessage());
        }
    }
}
