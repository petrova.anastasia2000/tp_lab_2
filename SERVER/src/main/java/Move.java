public class Move {
    private String message;
    private Type type;

    enum Type{
        PLAYER, SERVER_START, SERVER_END;
    }

    public Move(String message, Type type) {
        this.message = message;
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}